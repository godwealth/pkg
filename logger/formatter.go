package logger

import (
	"bytes"
	"fmt"
	"net/http"
	"strings"
	"sync"

	jsoniter "github.com/json-iterator/go"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

var (
	// MaxStackTrace 记录的错误信息的调用栈最大深度
	MaxStackTrace = 10

	_ logrus.Formatter = (*APPLogsV1Formatter)(nil)
	_ logrus.Formatter = (*HTTPRequestV1Formatter)(nil)

	emptyStack = make([]string, 0)

	appLogsV1Pool = sync.Pool{
		New: func() interface{} {
			return &APPLogsV1Data{
				Schema: string(APPLogsV1),
			}
		},
	}

	httpRequestV1Pool = sync.Pool{
		New: func() interface{} {
			return &HTTPRequestV1Data{
				Schema: string(HTTPRequestV1),
			}
		},
	}
)

// NewFormatter 获得日志规范对应的格式化对象
func NewFormatter(s Standard) (logrus.Formatter, error) {
	switch s {
	case APPLogsV1:
		return &APPLogsV1Formatter{
			TimeLayout: "2006-01-02T15:04:05.999999Z07:00",
		}, nil
	case HTTPRequestV1:
		return &HTTPRequestV1Formatter{
			TimeLayout: "2006-01-02T15:04:05.999999Z07:00",
		}, nil
	}

	return nil, errors.Wrapf(ErrFormatterNotFound, "log standard %q", s)
}

// APPLogsV1Data app.logs.v1日志输出内容
type APPLogsV1Data struct {
	Schema      string                 `json:"schema"`
	Time        string                 `json:"t"`
	Level       string                 `json:"l"`
	Service     string                 `json:"s"`
	Channel     string                 `json:"c"`
	Environment string                 `json:"e"`
	Message     string                 `json:"m"`
	Context     map[string]interface{} `json:"ctx"`
}

// APPLogsV1Formatter app.logs.v1日志格式化
type APPLogsV1Formatter struct {
	// 时间格式，默认ISO8601，精确到秒
	TimeLayout  string
	Service     string
	Environment string
}

// Format implements logrus.Formatter interface
func (af *APPLogsV1Formatter) Format(entry *logrus.Entry) ([]byte, error) {
	channel := ""
	context := logrus.Fields{}

	// 先处理caller记录，允许entry.Data内的数据覆盖caller
	// 可以实现自行记录caller的目的
	if entry.HasCaller() {
		caller := entry.Caller
		context[logrus.FieldKeyFile] = fmt.Sprintf("%s:%d", caller.File, caller.Line)
		context[logrus.FieldKeyFunc] = caller.Function
	}

	for k, v := range entry.Data {
		switch k {
		case "channel":
			channel, _ = v.(string)
		default:
			if err, ok := v.(error); !ok {
				context[k] = v
			} else {
				msg, trace := extractError(err)
				errData := logrus.Fields{
					"msg": msg,
				}
				if len(trace) > 0 {
					errData["trace"] = trace
				}
				context[k] = errData
			}
		}
	}

	data := appLogsV1Pool.Get().(*APPLogsV1Data)
	data.Time = entry.Time.Format(af.TimeLayout)
	data.Level = entry.Level.String()
	data.Service = af.Service
	data.Channel = channel
	data.Environment = af.Environment
	data.Message = entry.Message
	data.Context = context

	var b *bytes.Buffer
	if entry.Buffer != nil {
		b = entry.Buffer
	} else {
		b = &bytes.Buffer{}
	}

	if err := jsoniter.NewEncoder(b).Encode(data); err != nil {
		appLogsV1Pool.Put(data)
		return nil, errors.Wrapf(err, "json encode %s log", APPLogsV1)
	}

	appLogsV1Pool.Put(data)
	return b.Bytes(), nil
}

// HTTPRequestV1Data http.request.v1日志输出内容
type HTTPRequestV1Data struct {
	Schema      string            `json:"schema"`
	Service     string            `json:"service"`
	Environment string            `json:"environment"`
	Time        string            `json:"time"`
	IP          string            `json:"ip"`
	Method      string            `json:"method"`
	Path        string            `json:"path"`
	User        string            `json:"user"`
	Headers     map[string]string `json:"headers"`
	Get         logrus.Fields     `json:"get"`
	Post        logrus.Fields     `json:"post"`
	Extra       logrus.Fields     `json:"extra"`
}

// HTTPRequestV1Formatter http.request.v1日志格式化
type HTTPRequestV1Formatter struct {
	// 时间格式，默认ISO8601，精确到秒
	TimeLayout  string
	Service     string
	Environment string
}

// Format implements logrus.Formatter interface
func (hf *HTTPRequestV1Formatter) Format(entry *logrus.Entry) ([]byte, error) { // revive:disable-line
	rv, ok := entry.Data["request"]
	if !ok {
		return nil, errors.New(`require "request"`)
	}

	req, ok := rv.(*http.Request)
	if !ok {
		return nil, errors.New(`"request" type MUST be *http.Request`)
	}

	uid := ""
	extra := logrus.Fields{}
	for k, v := range entry.Data {
		switch k {
		case "request":
			continue
		case "user":
			uid = fmt.Sprintf("%v", v)
		default:
			if err, ok := v.(error); !ok {
				extra[k] = v
			} else {
				msg, trace := extractError(err)
				errData := logrus.Fields{
					"msg": msg,
				}
				if len(trace) > 0 {
					errData["trace"] = trace
				}
				extra[k] = errData
			}
		}
	}

	data := httpRequestV1Pool.Get().(*HTTPRequestV1Data)
	data.Service = hf.Service
	data.Environment = hf.Environment
	data.Time = entry.Time.Format(hf.TimeLayout)
	data.IP = parseIP(req.RemoteAddr)
	data.Method = req.Method
	data.Path = req.URL.Path
	data.User = uid
	data.Headers = map[string]string{}
	data.Get = logrus.Fields{}
	data.Post = logrus.Fields{}
	data.Extra = extra

	for k, v := range req.Header {
		k = strings.ToLower(k)
		if len(v) > 1 {
			data.Headers[k] = strings.Join(v, ", ")
		} else {
			data.Headers[k] = v[0]
		}
	}

	if q := req.URL.Query(); len(q) > 0 {
		for k, v := range q {
			if len(v) > 1 {
				data.Get[k] = v
			} else {
				data.Get[k] = v[0]
			}
		}
	}

	if vals := req.PostForm; len(vals) > 0 {
		for k, v := range vals {
			if len(v) > 1 {
				data.Post[k] = v
			} else {
				data.Post[k] = v[0]
			}
		}
	}

	var b *bytes.Buffer
	if entry.Buffer != nil {
		b = entry.Buffer
	} else {
		b = &bytes.Buffer{}
	}

	if err := jsoniter.NewEncoder(b).Encode(data); err != nil {
		httpRequestV1Pool.Put(data)
		return nil, errors.Wrapf(err, "json encode %s log", HTTPRequestV1)
	}

	httpRequestV1Pool.Put(data)
	return b.Bytes(), nil
}

type stackTracer interface {
	StackTrace() errors.StackTrace
}

// stackTrace 从错误信息中获取调用栈信息
func stackTrace(err error) []string {
	if err, ok := err.(stackTracer); ok {
		return strings.Split(
			strings.ReplaceAll(
				strings.TrimLeft(
					fmt.Sprintf("%+v", err.StackTrace()),
					"\n",
				),
				"\n\t",
				" ",
			),
			"\n",
		)
	}

	return emptyStack
}

func extractError(err error) (string, []string) {
	var trace []string
	if st := stackTrace(err); len(st) > 0 {
		if len(st) >= MaxStackTrace {
			st = st[:MaxStackTrace]
		}
		trace = st
	}
	return err.Error(), trace
}
