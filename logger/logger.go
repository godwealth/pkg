package logger

import (
	"fmt"
	"os"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// Standard 日志规范
type Standard string

const (
	// APPLogsV1 运行日志
	APPLogsV1 Standard = "app.logs.v1"
	// HTTPRequestV1 请求日志
	HTTPRequestV1 Standard = "http.request.v1"
)

var (
	// ErrFormatterNotFound 找不到对应规范的日志格式化对象
	ErrFormatterNotFound = fmt.Errorf("log formatter not found")
	loggers              = map[Standard]*logrus.Logger{}
	appLogsV1Entries     = map[string]*logrus.Entry{}
)

func init() {
	service := os.Getenv("SERVICE")
	env := os.Getenv("ENV")

	reqLogger, err := NewLogger(HTTPRequestV1)
	if err != nil {
		panic(errors.WithStack(err))
	}
	if f, ok := reqLogger.Formatter.(*HTTPRequestV1Formatter); ok {
		f.Service = service
		f.Environment = env
	}
	loggers[HTTPRequestV1] = reqLogger

	appLogger, err := NewLogger(APPLogsV1)
	if err != nil {
		panic(errors.WithStack(err))
	}
	if f, ok := appLogger.Formatter.(*APPLogsV1Formatter); ok {
		f.Service = service
		f.Environment = env
	}
	loggers[APPLogsV1] = appLogger

	if service == "" {
		Default().Warn("SERVICE environment variable is empty")
	}
	if env == "" {
		Default().Warn("ENV environment variable is empty")
	}

	if v := os.Getenv("LOG_LEVEL"); v != "" {
		if lvl, err := logrus.ParseLevel(v); err == nil {
			SetLevel(APPLogsV1, lvl)
		}
	}

	// 环境变量LOG_TO_SYSLOG=1，开启日志输出到syslog
	if os.Getenv("LOG_TO_SYSLOG") == "1" {
		for k, v := range loggers {
			if err := HookSyslog(v, string(k)); err != nil {
				panic(errors.WithStack(err))
			}
		}
	}

	// 环境变量LOG_TO_STDOUT=0, 关闭日志输出到标准输出
	if os.Getenv("LOG_TO_STDOUT") == "0" {
		for _, v := range loggers {
			if err := HookStdOut(v); err != nil {
				panic(errors.WithStack(err))
			}
		}
	}
}

// HTTPRequestV1Logger 请求日志
func HTTPRequestV1Logger() *logrus.Logger {
	return loggers[HTTPRequestV1]
}

// NewLogger 创建新的日志对象
func NewLogger(s Standard) (*logrus.Logger, error) {
	f, err := NewFormatter(s)
	if err != nil {
		return nil, err
	}

	l := logrus.New()
	l.SetFormatter(f)
	return l, nil
}

// SetLevel 设置日志级别
func SetLevel(s Standard, lvl logrus.Level) {
	if l, ok := loggers[s]; ok {
		l.SetLevel(lvl)
	}
}

// NewEntry 默认日志
func NewEntry(channel string) *logrus.Entry {
	return newAPPLogsV1Entry(channel)
}

// Default 默认日志
func Default() *logrus.Entry {
	return newAPPLogsV1Entry("default")
}

// Exception 日志
func Exception() *logrus.Entry {
	return newAPPLogsV1Entry("exception")
}

func newAPPLogsV1Entry(channel string) *logrus.Entry {
	if e, ok := appLogsV1Entries[channel]; ok {
		return e
	}

	e := logrus.NewEntry(loggers[APPLogsV1])
	if channel != "" {
		e = e.WithField("channel", channel)
	}
	appLogsV1Entries[channel] = e
	return e
}
