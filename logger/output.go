package logger

import (
	"bufio"
	sl "log/syslog"
	"os"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/syslog"
)

// HookSyslog 日志输出到syslog
func HookSyslog(l *logrus.Logger, tag string) error {
	h, err := syslog.NewSyslogHook("", "", sl.LOG_LOCAL6, tag)
	if err != nil {
		return errors.WithStack(err)
	}

	l.Hooks.Add(h)

	return nil
}

// HookSyslog 关闭日志输出到标准输出
func HookStdOut(l *logrus.Logger) error {
	src, err := os.OpenFile(os.DevNull, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		return errors.WithStack(err)
	}

	writer := bufio.NewWriter(src)
	l.SetOutput(writer)

	return nil
}
