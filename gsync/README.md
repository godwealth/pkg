## gsync
一个简单的goroutine限制池

### 安装
```
go get gitee.com/godwealth/pkg/gsync
```

### 使用
```
package main

import (
	"fmt"
	"time"

	"gitee.com/godwealth/pkg/gsync"
)

var wg = gsync.New(5)

func main() {
	userCount := 10
	for i := 0; i < userCount; i++ {
        wg.Add(1)
		go Read(i)
	}

	wg.Wait()
}

func Read(i int) {
	defer wg.Done()

	fmt.Printf("go func: %d, time: %d\n", i, time.Now().Unix())
	time.Sleep(time.Second)
}
```