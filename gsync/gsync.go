package gsync

import "sync"

type Signal struct {
	c  chan struct{}
	wg *sync.WaitGroup
}

func New(size int) *Signal {
	return &Signal{
		c:  make(chan struct{}, size),
		wg: new(sync.WaitGroup),
	}
}

func (s *Signal) Add(delta int) {
	s.wg.Add(delta)
	for i := 0; i < delta; i++ {
		s.c <- struct{}{}
	}
}

func (s *Signal) Done() {
	<-s.c
	s.wg.Done()
}

func (s *Signal) Wait() {
	s.wg.Wait()
}