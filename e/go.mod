module gitee.com/godwealth/pkg/e

go 1.16

require (
	gitee.com/godwealth/pkg/logger v0.0.0-20210317043256-48438ca536de
	github.com/gin-gonic/gin v1.6.3
	github.com/sirupsen/logrus v1.8.1
)
