## Golang gin错误处理

### Example
```
package main

import (
	"github.com/gin-gonic/gin"
	"gin-err-wrapper/pkg/e"
)

func ping(c *gin.Context) error {
    c.JSON(200, gin.H{
        "message": "pong",
    })
    return nil
}

func user(c *gin.Context) error {
    return e.ParameterError("userID参数有误")
}

func useradd(c *gin.Context) error {
	return e.ServerError()
}

func main() {

    r := gin.Default()
	
    r.GET("/ping", e.Wrapper(ping))
	r.GET("/user", e.Wrapper(user))
	r.GET("/adduser", e.Wrapper(useradd))
    r.Run(":5000") // 在 0.0.0.0:8080 上监听并服务
}
```
