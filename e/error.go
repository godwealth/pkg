package e

import (
	"net/http"
)

type APIException struct {
	Code      int    `json:"-"`
	ErrorCode int    `json:"error_code"`
	ErrorInfo error  `json:"-"`
	Msg       string `json:"msg"`
	Request   string `json:"request"`
}

func (e *APIException) Error() string {
	return e.Msg
}

func NewAPIException(code int, errorCode int, msg string, err error) *APIException {
	return &APIException{
		Code:      code,
		ErrorCode: errorCode,
		ErrorInfo: err,
		Msg:       msg,
	}
}

const (
	PARAMETER_ERROR = 1000 // 400错误
	AUTH_ERROR      = 1001 // 401错误
	UNKNOWN_ERROR   = 1003 // 403错误
	NOT_FOUND       = 1004 // 404错误
	SERVER_ERROR    = 1005 // 500错误
)

// 500 错误处理
func ServerError(message string, err error) *APIException {
	return NewAPIException(http.StatusInternalServerError, SERVER_ERROR, message, err)
}

// 404 错误
func NotFound(err error) *APIException {
	return NewAPIException(http.StatusNotFound, NOT_FOUND, http.StatusText(http.StatusNotFound), err)
}

// 403错误
func UnknownError(message string, err error) *APIException {
	return NewAPIException(http.StatusForbidden, UNKNOWN_ERROR, message, err)
}

// 400错误
func ParameterError(message string, err error) *APIException {
	return NewAPIException(http.StatusBadRequest, PARAMETER_ERROR, message, err)
}

// 401错误
func AuthError(message string, err error) *APIException {
	return NewAPIException(http.StatusUnauthorized, AUTH_ERROR, message, err)
}
