package e

import (
	"fmt"

	"gitee.com/godwealth/pkg/logger"
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
)

type HandlerFunc func(c *gin.Context) error

func Wrapper(handler HandlerFunc) func(c *gin.Context) {
	return func(c *gin.Context) {
		err := handler(c)
		if err != nil {
			apiException := err.(*APIException)

			apiException.Request = c.Request.Method + " " + c.Request.URL.String()
			logger.Exception().WithFields(logrus.Fields{
				"error_code": apiException.ErrorCode,
				"error":      fmt.Sprint(apiException.ErrorInfo),
			}).Info(apiException.Msg)
			
			c.JSON(apiException.Code, apiException)
			return
		}
	}
}
